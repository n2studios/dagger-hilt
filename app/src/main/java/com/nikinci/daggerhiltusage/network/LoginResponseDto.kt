package com.nikinci.daggerhiltusage.network

import com.nikinci.daggerhiltusage.model.User

/**
 * Created by nikinci on 2/3/21.
 */
data class LoginResponseDto(val user: User)