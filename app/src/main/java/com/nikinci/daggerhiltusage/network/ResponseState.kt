package com.nikinci.daggerhiltusage.network

/**
 * Created by nikinci on 2/3/21.
 */
sealed class ResponseState<out T> {
    data class Loading(val loading: Boolean) : ResponseState<Nothing>()
    data class Error(val errorMessage: String) : ResponseState<Nothing>()
    data class Success<T>(val data: T) : ResponseState<T>()
}