package com.nikinci.daggerhiltusage.network

/**
 * Created by nikinci on 2/3/21.
 */
data class LoginRequestDto(val username: String, val password: String)