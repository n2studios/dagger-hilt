package com.nikinci.daggerhiltusage.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier

/**
 * Created by nikinci on 2/3/21.
 */
@Module
@InstallIn(SingletonComponent::class)
class NetModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class BaseUrl

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class SSLCertificate

    @Provides
    @BaseUrl
    fun provideBaseUrl(): String {
        return "http://xxx.yyy"
    }

    @Provides
    @SSLCertificate
    fun provideSSLCertificate(): String {
        return "this is ssl certificate"
    }

}
