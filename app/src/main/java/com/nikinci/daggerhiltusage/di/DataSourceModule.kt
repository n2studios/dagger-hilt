package com.nikinci.daggerhiltusage.di

import com.nikinci.daggerhiltusage.data.remote.UserRemoteDataSource
import com.nikinci.daggerhiltusage.data.source.UserDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by nikinci on 2/3/21.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Binds
    abstract fun bindUserDataSource(
        //for mocking set PostMockDataSource
        dataSource: UserRemoteDataSource
    ): UserDataSource

}
