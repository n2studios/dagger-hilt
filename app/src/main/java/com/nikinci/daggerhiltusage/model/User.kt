package com.nikinci.daggerhiltusage.model

/**
 * Created by nikinci on 2/3/21.
 */
data class User(val userName: String, val userId: Long)