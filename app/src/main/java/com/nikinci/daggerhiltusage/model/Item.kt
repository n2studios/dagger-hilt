package com.nikinci.daggerhiltusage.model

/**
 * Created by nikinci on 2/3/21.
 */
data class Item(val message: String, val userName: Long)