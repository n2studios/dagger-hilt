package com.nikinci.daggerhiltusage.core

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by nikinci on 2/3/21.
 */
@HiltAndroidApp
class DaggerUsageApp : MultiDexApplication() {
}