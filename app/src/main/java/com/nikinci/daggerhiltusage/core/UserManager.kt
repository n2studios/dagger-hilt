package com.nikinci.daggerhiltusage.core

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nikinci.daggerhiltusage.model.User
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by nikinci on 2/3/21.
 */
@Singleton
class UserManager @Inject constructor() {
    private val _user =
        MutableLiveData<User?>()
    val user: LiveData<User?> =
        _user

    private val _authToken =
        MutableLiveData<String?>()
    val authToken: LiveData<String?> =
        _authToken


    fun setUser(user: User?) {
        _user.value = user
    }

    fun setAuthToken(token: String?) {
        _authToken.value = token
    }

    fun isLoggedIn() = _user.value != null
}