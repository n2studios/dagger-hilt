package com.nikinci.daggerhiltusage.data.source

import com.nikinci.daggerhiltusage.network.LoginResponseDto
import com.nikinci.daggerhiltusage.network.ResponseState
import kotlinx.coroutines.flow.Flow

/**
 * Created by nikinci on 2/3/21.
 */
interface UserDataSource {
    suspend fun login(username: String, password: String): Flow<ResponseState<LoginResponseDto>>
}