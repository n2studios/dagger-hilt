package com.nikinci.daggerhiltusage.data.local

import com.nikinci.daggerhiltusage.core.UserManager
import com.nikinci.daggerhiltusage.data.source.UserDataSource
import com.nikinci.daggerhiltusage.model.User
import com.nikinci.daggerhiltusage.network.LoginResponseDto
import com.nikinci.daggerhiltusage.network.ResponseState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by nikinci on 2/3/21.
 */
class UserMockDataSource @Inject constructor(var userManager: UserManager) : UserDataSource {
    override suspend fun login(
        username: String,
        password: String
    ): Flow<ResponseState<LoginResponseDto>> {
        val user = User(
            userName = "Local User",
            userId = Long.MAX_VALUE
        )
        return flow {
            emit(ResponseState.Loading(true))
            delay(2000)
            emit(
                ResponseState.Success(
                    LoginResponseDto(
                        user = user
                    )
                )
            )

            emit(ResponseState.Loading(false))
        }
    }
}
