package com.nikinci.daggerhiltusage.data.repository

import com.nikinci.daggerhiltusage.data.source.UserDataSource
import javax.inject.Inject

/**
 * Created by nikinci on 2/3/21.
 */

class UserRepository @Inject constructor(val dataSource: UserDataSource) : UserDataSource {
    override suspend fun login(username: String, password: String) =
        dataSource.login(username, password)
}