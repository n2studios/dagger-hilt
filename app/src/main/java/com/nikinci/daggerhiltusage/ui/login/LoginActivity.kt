package com.nikinci.daggerhiltusage.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.nikinci.daggerhiltusage.R
import com.nikinci.daggerhiltusage.core.UserManager
import com.nikinci.daggerhiltusage.databinding.ActivityLoginBinding
import com.nikinci.daggerhiltusage.network.ResponseState
import com.nikinci.daggerhiltusage.ui.home.ActivityModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Created by nikinci on 2/3/21.
 */
@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityLoginBinding

    val viewModel: LoginViewModel by viewModels()

    @Inject
    lateinit var userManager: UserManager


    @Inject
    lateinit var activityModel: ActivityModel


    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java).apply {
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        mBinding.lifecycleOwner = this
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding.apply {

            buttonLogin.setOnClickListener {
                viewModel.login(edittextUsername.text.toString(), edittextPassword.text.toString())
            }
        }

        viewModel.loginResponse.observe(this, Observer {
            when (it) {
                is ResponseState.Loading -> if (it.loading) {
                    //  show loading
                } else {
                    // hide loading
                }
                is ResponseState.Error -> {// showErrorDialog()
                    // show error
                }
                is ResponseState.Success -> {
                    val data = it.data
                    userManager.setUser(data.user)

                    val dialogBuilder = AlertDialog.Builder(this)
                    dialogBuilder.setMessage("Success")
                        .setCancelable(false)
                        .setPositiveButton(
                            ("Welcome beck ${data.user.userName}")
                        ) { dialog, id ->
                            dialog.dismiss()
                            finish()
                        }

                    val alert = dialogBuilder.create()
                    alert.setTitle("Login")
                    alert.show()
                }
            }

        })

        activityModel.activityState.observe(this, {
            mBinding?.textState.text = "Activity State: $it"
        })
        activityModel.setActiviytyState("login activity created")

        userManager.user.observe(this, {
            mBinding?.textUser.text = "Current User: ${it?.userName}"
        })


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}