package com.nikinci.daggerhiltusage.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.nikinci.daggerhiltusage.R
import com.nikinci.daggerhiltusage.core.UserManager
import com.nikinci.daggerhiltusage.di.NetModule
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {

    val homeViewModel: HomeViewModel by viewModels()

    @Inject
    @NetModule.BaseUrl
    lateinit var baseUrl: String

    @Inject
    @NetModule.SSLCertificate
    lateinit var sslKey: String


    @Inject
    lateinit var activityModel: ActivityModel

    @Inject
    lateinit var userManager: UserManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val textBaseUrl: TextView = root.findViewById(R.id.text_baseurl)
        val textSslPinning: TextView = root.findViewById(R.id.text_sslpinning)
        val textState: TextView = root.findViewById(R.id.text_state)
        val textUser: TextView = root.findViewById(R.id.text_user)

        textBaseUrl.text = "BaseUrl : $baseUrl"
        textSslPinning.text = "SSL Key : $sslKey"

        activityModel.activityState.observe(viewLifecycleOwner, {
            textState.text = "Activity State: $it"
        })

        userManager.user.observe(viewLifecycleOwner, {
            textUser.text = "User: ${it?.userName}"
        })



        activityModel.setActiviytyState("new state main activity")
        return root
    }
}