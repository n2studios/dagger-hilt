package com.nikinci.daggerhiltusage.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.nikinci.daggerhiltusage.R
import com.nikinci.daggerhiltusage.core.UserManager
import com.nikinci.daggerhiltusage.ui.home.ActivityModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class NotificationsFragment : Fragment() {

    val notificationsViewModel: NotificationsViewModel by viewModels()

    @Inject
    lateinit var activityModel: ActivityModel

    @Inject
    lateinit var userManager: UserManager


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_notifications, container, false)
        val textState: TextView = root.findViewById(R.id.text_state)
        val textUser: TextView = root.findViewById(R.id.text_user)

        activityModel.activityState.observe(viewLifecycleOwner, {
            textState.text = "Activity State: $it"
        })

        userManager.user.observe(viewLifecycleOwner, {
            textUser.text = "User: ${it?.userName}"
        })


        return root
    }
}