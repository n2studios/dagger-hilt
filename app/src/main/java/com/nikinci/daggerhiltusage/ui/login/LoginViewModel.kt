package com.nikinci.daggerhiltusage.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nikinci.daggerhiltusage.data.repository.UserRepository
import com.nikinci.daggerhiltusage.network.LoginResponseDto
import com.nikinci.daggerhiltusage.network.ResponseState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(var userRepository: UserRepository) : ViewModel() {


    private val _loginResponse =
        MutableLiveData<ResponseState<LoginResponseDto>>()
    val loginResponse: LiveData<ResponseState<LoginResponseDto>> =
        _loginResponse


    fun login(userName: String, password: String) {
        viewModelScope.launch {
            userRepository.login(userName, password).collect {
                _loginResponse.value = it
            }
        }

    }
}