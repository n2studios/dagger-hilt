package com.nikinci.daggerhiltusage.ui.home

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject


/**
 * Created by nikinci on 2/5/21.
 */
@ActivityScoped
class ActivityModel @Inject constructor(
    @ActivityContext var context: Context
) {


    private val _activityState =
        MutableLiveData<String>()
    val activityState: LiveData<String> =
        _activityState


    fun setActiviytyState(state: String) {
        _activityState.value = state
    }

}